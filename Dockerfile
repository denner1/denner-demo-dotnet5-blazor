FROM mcr.microsoft.com/dotnet/sdk:5.0 as build
WORKDIR /build
COPY BlazorApp.csproj .
RUN dotnet restore
COPY . .
CMD ["dotnet", "run", "--urls 'http://*:5000'"]
