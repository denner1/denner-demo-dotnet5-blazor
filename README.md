# Denner Demo Dotnet5 Blazor

This is a .net 5 blazor app with a couple of minor tweeks for docker.
This was created using the dotnet command  "dotnet new blazorserver -o BlazorApp --no-https"
Most of the code is from the template and should not be run in production as is.

Run in debug mode with the command "docker run --rm -it -p5000:5000 registry.gitlab.com/denner1/denner-demo-dotnet5-blazor"

